import './index.css'
import React from 'react'
import { applyMiddleware, createStore } from 'redux'
import thunk from 'redux-thunk'
import App from './App'
import { Provider } from 'react-redux'
import rootReducer from './reducers'
import { composeWithDevTools } from 'redux-devtools-extension'
import { render } from 'react-dom'

const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(thunk)),
)

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
)