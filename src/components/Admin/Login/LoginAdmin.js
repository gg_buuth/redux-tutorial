import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Redirect, useHistory } from "react-router-dom";
import { Form, Button, Card } from "react-bootstrap";


const LoginAdmin = () => {
    const history = useHistory();
    const [email, setEmail] = useState('');
    const [role, setRole] = useState('USER');

    const handleSubmit = () => {
        let data = {
            email: email,
            role: role
        }
        sessionStorage.clear();
        sessionStorage.setItem("email", JSON.stringify(data));
        if (role === 'USER') {
            history.push('/')
        } else {
            history.push('/admin')
        }
    }


    return (

        <Card style={{ width: '18rem' }}>
            <Card.Body>
                <Form>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" 
                            onChange={(e) => setEmail(e.target.value)} 
                            placeholder="Username"
                            value={email} placeholder="Enter email" />
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Role</Form.Label>
                        <Form.Control 
                            type="role" 
                            onChange={(e) => setRole(e.target.value)}
                            placeholder="Role"
                            value={role} /> 
                    </Form.Group>
                    <Button variant="primary" onClick={() => handleSubmit()} type="submit">
                        Submit
                    </Button>
                </Form>
            </Card.Body>
        </Card>
    )
}

export default LoginAdmin;