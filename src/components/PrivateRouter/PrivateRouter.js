import React from "react";
import { Redirect, Route } from "react-router-dom";


const PrivateRoute = ({ component: Component, roles, ...rest }) => (
  <Route {...rest} render={props => {
    const data = JSON.parse(sessionStorage.getItem("email"));
    console.log(data);
    if (!data) {
      // not logged in so redirect to login page with the return url
      return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    }

    // check if route is restricted by role
    if (roles && roles.indexOf(data.role) === -1) {
      // role not authorised so redirect to home page
      return <Redirect to={{ pathname: '/' }} />
    }

    // authorised so return component
    return <Component {...props} />
  }} />
)
export default PrivateRoute;