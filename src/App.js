import React, { useEffect } from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
} from 'react-router-dom'

import DashboardPage from './pages/DashboardPage'
import PostsPage from './pages/post/PostsPage'
import AddPost from './pages/post/AddPost'
import DetailPost from './pages/post/DetailPost'
import EditPost from './pages/post/EditPost'
import PrivateRoute from './components/PrivateRouter/PrivateRouter'
import HomeAdmin from './components/Admin/Home/HomeAdmin'
import LoginAdmin from './components/Admin/Login/LoginAdmin'
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './components/Home/Home'

const App = () => {

    useEffect(() => {
        console.log("Component Did mount, did update")

        return () => {
            console.log("Component Will Unmount")
        };
    }, []);

    return (
        <Router>
            <Switch>
                <Route exact path="/posts" component={DashboardPage} />
                <Route exact path="/" component={Home} />
                <Route exact path="/posts" component={PostsPage} />
                <Route exact path="/add-post" component={AddPost} />
                <Route path="/update-post/:id" component={EditPost} />
                <Route path="/detail-post/:id" component={DetailPost} />

                <PrivateRoute path="/admin" roles={['ADMIN']} component={HomeAdmin} />
                <Route path="/login" component={LoginAdmin} />
            </Switch>
        </Router>
    )
}

export default App