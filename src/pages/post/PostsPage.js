import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchPosts } from '../../actions/postsActions'
import { Post } from './Post'
import { Link } from 'react-router-dom'

// Redux state is now in the props of the component
const PostsPage = () => {

    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(fetchPosts())
    }, [dispatch])

    const posts = useSelector(state => state.posts.posts)
    const loading = useSelector(state => state.posts.loading)
    const hasErrors = useSelector(state => state.posts.hasErrors)

    const renderPosts = () => {
        if (loading) return <p>Loading posts...</p>
        if (hasErrors) return <p>Unable to display posts.</p>
        return posts.map((post) => <Post key={post.id} post={post} />)
    }

    return (
        <section>
            <h1>Posts</h1>
            <Link to="/add-post">Thêm mới post</Link>
            {renderPosts()}
        </section>
    )
}

export default PostsPage