import React from 'react'
import { Link } from 'react-router-dom'

export const Post = ({ post }) => (
    <article className="post-excerpt">
        <h2>{post.title}</h2>
        <p>{post.body.substring(0, 80)}</p>
        <Link target="_blank" to={`/detail-post/${post.id}`}>Detail</Link>
        <Link target="_blank" to={`/update-post/${post.id}`}> Update</Link>
    </article>
)