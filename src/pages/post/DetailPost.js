import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';

const DetailPost = () => {

    const initialFormState = { id: 0, userId: 1, title: '', body: '' }
    const [post, setPost] = useState(initialFormState)

    //const location = useLocation();
    //const history = useHistory();
    const params = useParams();
    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch('https://jsonplaceholder.typicode.com/posts/' + params.id)
            const data = await response.json()
            setPost(data);
            // console.log(data)
        };

        fetchData();

    }, [params]);

    return (
        <div>

            <div>id: {post.id}</div>
            <div>userId: {post.userId}</div>
            <div>title: {post.title}</div>
            <div>body: {post.body}</div>
        </div>
    )
}

export default DetailPost