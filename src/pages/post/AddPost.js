import React, { useState, useEffect } from 'react'
import { useHistory } from "react-router"

import { addPost } from '../../actions/postsActions'
import { useDispatch } from 'react-redux'
import { useForm } from 'react-hook-form';

const AddPost = () => {
    const dispatch = useDispatch()
    const history = useHistory()

    const initialFormState = { userId: 1, title: '', body: '', age: 0 }
    const [post, setPost] = useState(initialFormState)
    const [adding, setAdding] = useState(false)


    useEffect(() => {
        console.log(dispatch)
    }, [dispatch])



    const { register, handleSubmit, errors } = useForm(); // initialise the hook
    const onSubmit = (dataFrom) => {
        console.log(dataFrom);

        setAdding({ ...adding, adding: true })

        const data = dispatch(addPost(dataFrom)).then(res => {
            console.log(res)
            setAdding({ ...adding, adding: false })
            history.push("/posts")
        })
        console.log(data)
        setPost(initialFormState)
    };

    const renderGiaoDiens = () => {
        //if (adding) return <p>Adding posts...</p>

        return (
            <form onSubmit={handleSubmit(onSubmit)}>
                <label>Name</label>
                <input
                    type="text"
                    name="title"
                    ref={register}
                />
                {errors.title && 'Please enter title.'}
                <label>body</label>
                <input
                    type="text"
                    name="body"
                    ref={register({ required: true })}
                />
                {errors.body && <p style={{ color: 'red' }}>Please enter body.</p>}
                <label>Age</label>
                <input
                    type="number"
                    name="age"
                    ref={register({ max: 100, min: 0 })}
                />
                {errors.age && 'Please enter age.'}
                <br />
                <button type="submit" disabled={adding}>Add new user</button>
            </form>
        )
    }

    return (
        <div>
            {renderGiaoDiens()}
        </div>
    )
}

export default AddPost