import React, { useState, useEffect } from 'react'
import { useHistory, useParams } from "react-router"

import { editPost } from '../../actions/postsActions'
import { useDispatch } from 'react-redux'

const EditPost = () => {
    const dispatch = useDispatch()
    const history = useHistory()
    const params = useParams();
    const initialFormState = { id: 0, userId: 1, title: '', body: '' }
    const [post, setPost] = useState(initialFormState)

    const handleInputChange = (event) => {
        const { name, value } = event.target
        setPost({ ...post, [name]: value })
    }

    useEffect(() => {
        console.log(dispatch)
        const fetchData = async () => {
            const response = await fetch('https://jsonplaceholder.typicode.com/posts/' + params.id)
            const data = await response.json()
            setPost(data);
            console.log(data)
        };

        fetchData();
    }, [dispatch, params])

    const editUser = async (post) => {

        post.id = params.id
        dispatch(editPost(params.id, post)).then(res => {
            console.log(res)
            history.push("/posts")
        })

    }

    return (
        <form
            onSubmit={event => {
                event.preventDefault()
                if (!post.title || !post.body) return
                editUser(post)
                setPost(initialFormState)
            }}
        >
            <label>Name</label>
            <input
                type="text"
                name="title"
                onChange={handleInputChange}
                value={post.title} />
            <label>body</label>
            <input
                type="text"
                name="body"
                onChange={handleInputChange}
                value={post.body} />
            <button>Add new user</button>
        </form>
    )
}

export default EditPost