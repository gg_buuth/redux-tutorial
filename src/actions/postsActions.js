// Create Redux action types
export const GET_POSTS = 'GET_POSTS'
export const GET_POST = 'GET_POST'
export const GET_POSTS_SUCCESS = 'GET_POSTS_SUCCESS'
export const GET_POSTS_FAILURE = 'GET_POSTS_FAILURE'
export const ADD_POSTS_FAILURE = 'ADD_POSTS_FAILURE'
export const EDIT_POSTS_FAILURE = 'EDIT_POSTS_FAILURE'

// Create Redux action creators that return an action
export const getPosts = () => ({
    type: GET_POSTS,
})

export const getPost = () => ({
    type: GET_POST,
})

export const getPostsSuccess = (posts) => ({
    type: GET_POSTS_SUCCESS,
    payload: posts,
})

export const getPostsFailure = () => ({
    type: GET_POSTS_FAILURE,
})

export const getPostFailure = () => ({
    type: GET_POSTS_FAILURE,
})

export const addPostsFailure = () => ({
    type: ADD_POSTS_FAILURE,
})

export const editPostsFailure = () => ({
    type: EDIT_POSTS_FAILURE,
})


// Combine them all in an asynchronous thunk
export function fetchPosts() {
    return async (dispatch) => {
        dispatch(getPosts())

        try {
            const response = await fetch('https://jsonplaceholder.typicode.com/posts')
            const data = await response.json()

            dispatch(getPostsSuccess(data))
        } catch (error) {
            dispatch(getPostsFailure())
        }
    }
}

export function fetchPost(id) {
    return async (dispatch) => {
        dispatch(getPosts())

        try {
            const response = await fetch('https://jsonplaceholder.typicode.com/posts/' + id)
            const data = await response.json()

            return data;
        } catch (error) {
            dispatch(getPostFailure())
        }
    }
}

export function addPost(post) {
    return async (dispatch) => {
        try {
            const response = await fetch('https://jsonplaceholder.typicode.com/posts', {
                method: 'POST',
                body: JSON.stringify({
                    title: post.title,
                    body: post.body,
                    userId: post.userId
                }),
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            })
            const data = await response.json()

            return data;
        } catch (error) {
            dispatch(addPostsFailure())
        }
    }
}

export function editPost(id, post) {
    return async (dispatch) => {
        try {
            const response = await fetch('https://jsonplaceholder.typicode.com/posts/' + id, {
                method: 'PUT',
                body: JSON.stringify({
                    id: id,
                    title: post.title,
                    body: post.body,
                    userId: post.userId
                }),
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            })
            const data = await response.json()

            return data;
        } catch (error) {
            dispatch(addPostsFailure())
        }
    }
}